<?php

// require_once 'vendor/hybridauth/hybridauth/src/Hybridauth.php';
// require_once './application/config/gmail.php';

// require_once('config.php');

use Hybridauth\Provider\Google;

class CallBack extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_access_token');
        $this->load->config('gmail');
        $this->load->database();
    }
    public function index()
    {

        try {
            $adapter = new Google($config = [
                'callback' => 'http://localhost/testmail/callback',
                'keys'     => [
                    'id' => '678796338207-nam8e0746ttj8sn1eno6arjqh41qqech.apps.googleusercontent.com',
                    'secret' => 'uGon7-PUYtt5KtSa4usShJjY'
                ],
                'scope'    => 'https://mail.google.com',
                'authorize_url_parameters' => [
                    'approval_prompt' => 'force', // to pass only when you need to acquire a new refresh token.
                    'access_type' => 'offline'
                ]
            ]);
            $adapter->authenticate();

            // var_dump($adapter->getAccessToken());
            $aa = $this->M_access_token->is_token_empty();
            // var_dump($aa);
            // die;
            if ($this->M_access_token->is_token_empty() == 0) {
                $this->M_access_token->update_access_token(json_encode($this->M_access_token->get_access_token()));
                echo "Access token inserted successfully.";
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
