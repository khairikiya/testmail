<?php
send_email_to_user('skiade07@gmail.com');

class M_access_token extends CI_Controller
{
    public function __construct()
    {
        $this->load->model('M_access_token');
    }
    function send_email_to_user($email)
    {
        require_once 'config.php';

        // $db = new DB();
        $arr_token = (array) $this->M_access_token->get_access_token();

        try {
            $transport = (new Swift_SmtpTransport('smtp.googlemail.com', 465, 'ssl'))
                ->setAuthMode('XOAUTH2')
                ->setUsername('khairikiya@gmail.com')
                ->setPassword($arr_token['access_token']);

            // Create the Mailer using your created Transport
            $mailer = new Swift_Mailer($transport);

            // Create a message
            $body = 'Hello, <p>Email sent through <span style="color:red;">Swift Mailer</span>.</p>';

            $message = (new Swift_Message('Email Through Swift Mailer'))
                ->setFrom(['khairikiya@gmail.com' => 'Khairi Kiya'])
                ->setTo([$email])
                ->setBody($body)
                ->setContentType('text/html');

            // Send the message
            $mailer->send($message);

            echo 'Email has been sent.';
        } catch (Exception $e) {
            if (!$e->getCode()) {
                $refresh_token = $this->M_access_token->get_refersh_token();

                $response = $this->adapter->refreshAccessToken([
                    "grant_type" => "refresh_token",
                    "refresh_token" => $refresh_token,
                    "client_id" => GOOGLE_CLIENT_ID,
                    "client_secret" => GOOGLE_CLIENT_SECRET,
                ]);

                $data = (array) json_decode($response);
                $data['refresh_token'] = $refresh_token;

                $this->M_access_token->update_access_token(json_encode($data));

                send_email_to_user($email);
            } else {
                echo $e->getMessage(); //print the error
            }
        }
    }
}
